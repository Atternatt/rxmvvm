/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Interface representing the api calls for {@link Retrofit}.
 *
 * @author Marc Moreno
 * @version 1.0
 */

public interface RestApi {

    /** The app id, need to identify the client.*/
    String APP_ID = "m2f_mvvm_android_test";

    /**The endpoint.*/
    String BASE_URL = "http://api.bandsintown.com/";

    /**
     * Given an artist name return a list of events.
     * @param artist the artist
     * @return a {@link Observable<EventResponse>} that emit EventResponses.
     */
    @GET("artists/{artist}/events.json?api_version=2.0&app_id=" + APP_ID)
    Observable<Result<EventResponse>> fetchEvents(@Path("artist") String artist);
}
