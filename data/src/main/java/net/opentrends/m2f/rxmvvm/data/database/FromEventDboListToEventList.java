/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.database;

import net.opentrends.m2f.rxmvvm.data.database.model.EventDBO;
import net.opentrends.m2f.rxmvvm.data.database.model.mapper.EventDboMapper;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

import java.util.List;

import rx.Observable;

/**
 * {@link Observable.Transformer} implementation that transforms a {@link List<EventDBO>} to a {@link List<Event>}
 * using {@link EventDboMapper}.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class FromEventDboListToEventList implements Observable.Transformer<List<EventDBO>, List<Event>> {

  @Override public Observable<List<Event>> call(Observable<List<EventDBO>> listObservable) {
    return listObservable.flatMap(Observable::from)
        .map(EventDboMapper::transformToBO)
        .toList();
  }
}
