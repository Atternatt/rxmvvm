/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.api;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.Result;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Proxy implementation of {@link RestApi}.
 * @author Marc Moreno
 * @version 1.0
 */
@Singleton
public class RestApiImpl implements RestApi {

    /**The service.*/
    RestApi service;

    @Inject
    public RestApiImpl() {
        configureApi();
    }

    /**
     * Configure the service.
     */
    private void configureApi() {
        service = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(RestApi.class);
    }

    /**
     * {@inheritDoc}
     */
    @RxLogObservable
    @Override
    public Observable<Result<EventResponse>> fetchEvents(String artist) {
        // TODO: 7/04/16 onErrorResumeNext tendria que propagar el error si es diferente de un 404
        return Observable.defer(() -> service.fetchEvents(artist))
                .filter(eventResponseResult -> !eventResponseResult.isError() && eventResponseResult.response().isSuccessful())
                .onErrorResumeNext(Observable.never())
                .subscribeOn(Schedulers.io());
    }
}
