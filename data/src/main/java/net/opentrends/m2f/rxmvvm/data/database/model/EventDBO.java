/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.database.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Marc on 10/4/16.
 */
@DatabaseTable(tableName = EventDBO.TABLE_NAME)
public class EventDBO extends BaseEntity{

  public static final String TABLE_NAME = "event_cache";

  public static final String FIELD_ID = "id";
  public static final String ARTIST_NAME = "artistName";
  public static final String TITLE = "title";
  public static final String DATETIME = "datetime";
  public static final String FORMATTED_DATETIME = "formattedDatetime";
  public static final String FORMATTED_LOCATION = "formattedLocation";
  public static final String TICKET_URL = "ticketUrl";
  public static final String TICKET_TYPE = "ticketType";
  public static final String TICKET_STATUS = "ticketStatus";
  public static final String ON_SALE_DATETIME = "onSaleDatetime";
  public static final String FACEBOOK_RSVP_URL = "facebookRsvpUrl";
  public static final String DESCRIPTION = "description";

  @DatabaseField(columnName = FIELD_ID, generatedId = true)
  private int id;

  @DatabaseField(columnName = TITLE)
  private String title;
  @DatabaseField(columnName = DATETIME)
  private String datetime;
  @DatabaseField(columnName = FORMATTED_DATETIME)
  private String formattedDatetime;
  @DatabaseField(columnName = FORMATTED_LOCATION)
  private String formattedLocation;
  @DatabaseField(columnName = TICKET_URL)
  private String ticketUrl;
  @DatabaseField(columnName = TICKET_TYPE)
  private String ticketType;
  @DatabaseField(columnName = TICKET_STATUS)
  private String ticketStatus;
  @DatabaseField(columnName = ON_SALE_DATETIME)
  private String onSaleDatetime;
  @DatabaseField(columnName = FACEBOOK_RSVP_URL)
  private String facebookRsvpUrl;
  @DatabaseField(columnName = DESCRIPTION)
  private String description;

  @DatabaseField(columnName = ARTIST_NAME)
  private String artistName;

  public EventDBO() {
  }

  public EventDBO(int id, String title, String datetime, String formattedDatetime,
                  String formattedLocation, String ticketUrl, String ticketType, String ticketStatus,
                  String onSaleDatetime, String facebookRsvpUrl, String description) {
    this.id = id;
    this.title = title;
    this.datetime = datetime;
    this.formattedDatetime = formattedDatetime;
    this.formattedLocation = formattedLocation;
    this.ticketUrl = ticketUrl;
    this.ticketType = ticketType;
    this.ticketStatus = ticketStatus;
    this.onSaleDatetime = onSaleDatetime;
    this.facebookRsvpUrl = facebookRsvpUrl;
    this.description = description;

    this.artistName = "";
  }

  public int getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getDatetime() {
    return datetime;
  }

  public String getFormattedDatetime() {
    return formattedDatetime;
  }

  public String getFormattedLocation() {
    return formattedLocation;
  }

  public String getTicketUrl() {
    return ticketUrl;
  }

  public String getTicketType() {
    return ticketType;
  }

  public String getTicketStatus() {
    return ticketStatus;
  }

  public String getOnSaleDatetime() {
    return onSaleDatetime;
  }

  public String getFacebookRsvpUrl() {
    return facebookRsvpUrl;
  }

  public String getDescription() {
    return description;
  }

  public String getArtistName() {
    return artistName;
  }

  public void setArtistName(String artistName) {
    this.artistName = artistName;
  }
}
