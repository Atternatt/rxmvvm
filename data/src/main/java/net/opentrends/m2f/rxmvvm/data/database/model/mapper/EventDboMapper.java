/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.database.model.mapper;

import net.opentrends.m2f.rxmvvm.data.database.model.EventDBO;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

/**
 * Mapper class that transforms a database layer event into domain layer object.
 *
 * @author marc moreno
 * @version 1.0
 */
public class EventDboMapper {

  /**
   * Transforms a {@link EventDBO} to {@link Event}
   * @param event a {@link EventDBO}
   * @return a {@link Event}
   */
  public static Event transformToBO(final EventDBO event) {
    return new Event(event.getId(),
        event.getTitle(),
        event.getDatetime(),
        event.getFormattedDatetime(),
        event.getFormattedLocation(),
        event.getTicketUrl(),
        event.getTicketType(),
        event.getTicketStatus(),
        event.getOnSaleDatetime(),
        event.getFacebookRsvpUrl(),
        event.getDescription());
  }

  public static EventDBO transformFromBO(final Event event) {
    return new EventDBO(event.getId(),
            event.getTitle(),
            event.getDatetime(),
            event.getFormattedDatetime(),
            event.getFormattedLocation(),
            event.getTicketUrl(),
            event.getTicketType(),
            event.getTicketStatus(),
            event.getOnSaleDatetime(),
            event.getFacebookRsvpUrl(),
            event.getDescription());
  }
}
