/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.api.entity.mapper;

import net.opentrends.m2f.rxmvvm.data.api.entity.EventEntity;
import net.opentrends.m2f.rxmvvm.data.database.model.EventDBO;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

/**
 * Mapper class that transforms a rest layer event into domain layer object.
 *
 * @author marc moreno
 * @version 1.0
 */
public final class EventEntityMapper {

    /**
     * transforms a {@link EventEntity} to {@link Event}
     * @param eventEntity a {@link EventEntity}
     * @return a {@link Event}
     */
    public static Event transformToBO(final EventEntity eventEntity) {
        return new Event(eventEntity.getId(),
                eventEntity.getTitle(),
                eventEntity.getDatetime(),
                eventEntity.getFormattedDatetime(),
                eventEntity.getFormattedLocation(),
                eventEntity.getTicketUrl(),
                eventEntity.getTicketType(),
                eventEntity.getTicketStatus(),
                eventEntity.getOnSaleDatetime(),
                eventEntity.getFacebookRsvpUrl(),
                eventEntity.getDescription());
    }

    /**
     * transforms a {@link EventEntity} to {@link EventDBO}
     * @param eventEntity a {@link EventEntity}
     * @return a {@link EventDBO}
     */
    public static EventDBO transformToDBO(final EventEntity eventEntity) {
        return new EventDBO(eventEntity.getId(),
            eventEntity.getTitle(),
            eventEntity.getDatetime(),
            eventEntity.getFormattedDatetime(),
            eventEntity.getFormattedLocation(),
            eventEntity.getTicketUrl(),
            eventEntity.getTicketType(),
            eventEntity.getTicketStatus(),
            eventEntity.getOnSaleDatetime(),
            eventEntity.getFacebookRsvpUrl(),
            eventEntity.getDescription());
    }
}
