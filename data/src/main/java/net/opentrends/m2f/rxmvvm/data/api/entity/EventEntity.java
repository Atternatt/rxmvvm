/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.api.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Data layer Event representation.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class EventEntity {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("formatted_datetime")
    @Expose
    private String formattedDatetime;
    @SerializedName("formatted_location")
    @Expose
    private String formattedLocation;
    @SerializedName("ticket_url")
    @Expose
    private String ticketUrl;
    @SerializedName("ticket_type")
    @Expose
    private String ticketType;
    @SerializedName("ticket_status")
    @Expose
    private String ticketStatus;
    @SerializedName("on_sale_datetime")
    @Expose
    private String onSaleDatetime;
    @SerializedName("facebook_rsvp_url")
    @Expose
    private String facebookRsvpUrl;
    @SerializedName("description")
    @Expose
    private String description;

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     *
     * @param datetime
     * The datetime
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     *
     * @return
     * The formattedDatetime
     */
    public String getFormattedDatetime() {
        return formattedDatetime;
    }

    /**
     *
     * @param formattedDatetime
     * The formatted_datetime
     */
    public void setFormattedDatetime(String formattedDatetime) {
        this.formattedDatetime = formattedDatetime;
    }

    /**
     *
     * @return
     * The formattedLocation
     */
    public String getFormattedLocation() {
        return formattedLocation;
    }

    /**
     *
     * @param formattedLocation
     * The formatted_location
     */
    public void setFormattedLocation(String formattedLocation) {
        this.formattedLocation = formattedLocation;
    }

    /**
     *
     * @return
     * The ticketUrl
     */
    public String getTicketUrl() {
        return ticketUrl;
    }

    /**
     *
     * @param ticketUrl
     * The ticket_url
     */
    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    /**
     *
     * @return
     * The ticketType
     */
    public String getTicketType() {
        return ticketType;
    }

    /**
     *
     * @param ticketType
     * The ticket_type
     */
    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    /**
     *
     * @return
     * The ticketStatus
     */
    public String getTicketStatus() {
        return ticketStatus;
    }

    /**
     *
     * @param ticketStatus
     * The ticket_status
     */
    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    /**
     *
     * @return
     * The onSaleDatetime
     */
    public String getOnSaleDatetime() {
        return onSaleDatetime;
    }

    /**
     *
     * @param onSaleDatetime
     * The on_sale_datetime
     */
    public void setOnSaleDatetime(String onSaleDatetime) {
        this.onSaleDatetime = onSaleDatetime;
    }

    /**
     *
     * @return
     * The facebookRsvpUrl
     */
    public String getFacebookRsvpUrl() {
        return facebookRsvpUrl;
    }

    /**
     *
     * @param facebookRsvpUrl
     * The facebook_rsvp_url
     */
    public void setFacebookRsvpUrl(String facebookRsvpUrl) {
        this.facebookRsvpUrl = facebookRsvpUrl;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

}