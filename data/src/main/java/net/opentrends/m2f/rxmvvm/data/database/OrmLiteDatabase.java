/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.database;

import android.content.Context;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.j256.ormlite.dao.Dao;

import net.opentrends.m2f.rxmvvm.data.database.helper.OrmLiteDatabaseHelper;
import net.opentrends.m2f.rxmvvm.data.database.model.EventDBO;
import net.opentrends.m2f.rxmvvm.data.database.model.mapper.EventDboMapper;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * @author Marc Moreno
 * @version 1.0
 */
@Singleton
public class OrmLiteDatabase implements EventDatabase {

    private OrmLiteDatabaseHelper helper;
    private Context context;

    @Inject
    public OrmLiteDatabase(Context context) {
        this.context = context;
    }

    public OrmLiteDatabaseHelper getHelper() {
        if (helper == null) {
            helper = new OrmLiteDatabaseHelper(this.context);
        }
        return helper;
    }

    @RxLogObservable
    @Override
    public Observable<List<EventDBO>> fetchEvents(String artist) {

        Observable<List<EventDBO>> observable = Observable.create(subscriber -> {
                    try {
                        List<EventDBO> events = getHelper().getDao().queryBuilder().where().eq("artistName", artist).query();
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(events);
                            subscriber.onCompleted();
                        }
                    } catch (SQLException e) {
                        subscriber.onError(e);
                    }
                    // TODO: 14/04/16 aixo no funciona correctament, provoca errors com 'attempt to re-open an already-closed object: SQLiteDatabase
//                    subscriber.add(Subscriptions.create(() -> helper.close()));
                }
        );

        observable
                .subscribeOn(Schedulers.io());

        return observable;
    }

    @Override
    public void storeEventListForArtist(List<Event> list, String artistName) {
        for (Event event : list) {
            storeEventForArtist(event, artistName);
        }
    }

    @Override
    public void storeEventForArtist(Event event, String artistName) {
        try {
            Dao<EventDBO, Integer> dao = getHelper().getDao();
            EventDBO eventDBO = EventDboMapper.transformFromBO(event);
            eventDBO.setArtistName(artistName);
            dao.create(eventDBO);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @RxLogObservable
    @Override
    public Observable<List<String>> fetchArtists() {
        throw new RuntimeException("Not yet implemented");
    }
}
