/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.database;

import net.opentrends.m2f.rxmvvm.data.database.model.EventDBO;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

import java.util.List;

import rx.Observable;

/**
 * Event DataBase representation.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public interface EventDatabase {


    /**
     * Given an artist name return a list of events.
     *
     * @param artist the artist
     * @return a {@link Observable} that emit a list of {@link EventDBO}.
     */
    Observable<List<EventDBO>> fetchEvents(String artist);

    /**
     * Store a list of events.
     *
     * @param list       the list of events.
     * @param artistName the name of the artist
     */
    void storeEventListForArtist(final List<Event> list, String artistName);

    /**
     * Store a single event.
     *
     * @param event      the event
     * @param artistName tha name of the artist
     */
    void storeEventForArtist(final Event event, String artistName);

    /**
     * Get a list of Artist names stored in the database.
     *
     * @return a Observable that emits artist name lists
     */
    Observable<List<String>> fetchArtists();

}
