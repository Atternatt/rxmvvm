/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.data.repository.datasource;

import net.opentrends.m2f.rxmvvm.data.api.FromEventResponseToEventListWithDatabaseStoring;
import net.opentrends.m2f.rxmvvm.data.api.RestApi;
import net.opentrends.m2f.rxmvvm.data.database.EventDatabase;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

import java.util.List;

import rx.Observable;

/**
 * Cloud implementation of {@link EventDataStore}.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class CloudDataStore implements EventDataStore {

    RestApi api;
    EventDatabase database;

    public CloudDataStore(RestApi api, EventDatabase database) {
        this.api = api;
        this.database = database;
    }

    /**
   * {@inheritDoc}
   */
    @Override
    public Observable<List<Event>> eventList(String searchQuery) {
        Observable<List<Event>> observable = api.fetchEvents(searchQuery)
                .compose(new FromEventResponseToEventListWithDatabaseStoring(database, searchQuery));

        return observable;

    }
}
