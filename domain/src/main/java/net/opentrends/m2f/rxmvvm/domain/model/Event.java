/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.domain.model;

/**
 * Model class that represents a Event.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class Event {

    private final int id;
    private final String title;
    private final String datetime;
    private final String formattedDatetime;
    private final String formattedLocation;
    private final String ticketUrl;
    private final String ticketType;
    private final String ticketStatus;
    private final String onSaleDatetime;
    private final String facebookRsvpUrl;
    private final String description;

    public Event(int id,
                 String title,
                 String datetime,
                 String formattedDatetime,
                 String formattedLocation,
                 String ticketUrl, String ticketType,
                 String ticketStatus,
                 String onSaleDatetime,
                 String facebookRsvpUrl,
                 String description) {
        this.id = id;
        this.title = title;
        this.datetime = datetime;
        this.formattedDatetime = formattedDatetime;
        this.formattedLocation = formattedLocation;
        this.ticketUrl = ticketUrl;
        this.ticketType = ticketType;
        this.ticketStatus = ticketStatus;
        this.onSaleDatetime = onSaleDatetime;
        this.facebookRsvpUrl = facebookRsvpUrl;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getFormattedDatetime() {
        return formattedDatetime;
    }

    public String getFormattedLocation() {
        return formattedLocation;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public String getTicketType() {
        return ticketType;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public String getOnSaleDatetime() {
        return onSaleDatetime;
    }

    public String getFacebookRsvpUrl() {
        return facebookRsvpUrl;
    }

    public String getDescription() {
        return description;
    }
}