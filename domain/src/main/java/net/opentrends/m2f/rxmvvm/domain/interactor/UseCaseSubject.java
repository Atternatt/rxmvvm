/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.domain.interactor;

import net.opentrends.m2f.rxmvvm.domain.executor.PostExecutionThread;
import net.opentrends.m2f.rxmvvm.domain.retrypolicy.RetryPolicy;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subjects.Subject;
import rx.subscriptions.Subscriptions;

/**
 * Abstract class that represents a use case, this one has a particularity:
 * It acts as a Observer and a Observable at the same time so it can listen
 * for a Observable emisions and can emit items as well. See {@link Subject}.
 * <br>
 * A UseCase is a {@link Observable.Transformer} that can be composed to any
 * observable. So a UseCase must be used to transform data from a data source
 * or repository.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public abstract class UseCaseSubject<T, R> implements Observable.Transformer<T, R> {

    private final PostExecutionThread postExecutionThread;

    private RetryPolicy retryPolicy;

    private Subscription subscription = Subscriptions.empty();

    protected UseCaseSubject(PostExecutionThread postExecutionThread) {
        this.postExecutionThread = postExecutionThread;
    }

    /**
     * Binds the entry parameters with a subscriber
     *
     * @param observable        the obsarvable that will bring the entry parameters.
     * @param useCaseSubscriber The guy who will be listen to the observable.
     */
    @SuppressWarnings("unchecked")
    public void bind(final Observable<T> observable, final Subscriber useCaseSubscriber) {
        this.subscription = observable
                .observeOn(postExecutionThread.getScheduler())
                .compose(this)
                .retryWhen(retryPolicy)
                .subscribe(useCaseSubscriber);
    }

    /**
     * Unsubscribes from current {@link rx.Subscription}.
     */
    public void unsubscribe() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    public void setRetryPolicy(RetryPolicy retryPolicy) {
        this.retryPolicy = retryPolicy;
    }

    @Override
    public abstract Observable<R> call(Observable<T> tObservable);
}
