/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.app.eventlist;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import io.fabric.sdk.android.Fabric;
import javax.inject.Inject;
import net.opentrends.m2f.rxmvvm.R;
import net.opentrends.m2f.rxmvvm.app.ui.BaseActivity;
import net.opentrends.m2f.rxmvvm.databinding.ActivityEventListBinding;
import net.opentrends.m2f.rxmvvm.di.components.DaggerEventComponent;

/**
 * Activity that shows a list of Events.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class EventListActivity extends BaseActivity {

  @Inject EventListViewModel eventListViewModel;

  private ActivityEventListBinding binding;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Fabric fabric =
        new Fabric.Builder(this)
            .kits(new Crashlytics(), new Answers())
            .debuggable(true)
            .build();

    Fabric.with(fabric);

    binding = DataBindingUtil.setContentView(this, R.layout.activity_event_list);

    initializeInjector();

    binding.setEventViewModel(eventListViewModel);
  }

  @Override protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    binding.toolbar.inflateMenu(R.menu.search);
    binding.searchView.setMenuItem(binding.toolbar.getMenu().findItem(R.id.action_search));
  }

  private void initializeInjector() {
    DaggerEventComponent.builder()
        .applicationComponent(getApplicationComponent())
        .activityModule(getActivityModule())
        .build()
        .inject(this);
  }
}
