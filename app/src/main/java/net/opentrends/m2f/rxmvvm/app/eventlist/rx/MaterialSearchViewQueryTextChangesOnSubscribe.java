/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.app.eventlist.rx;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import rx.Observable;
import rx.Subscriber;
import rx.android.MainThreadSubscription;

/**
 * {@link Observable.OnSubscribe} implementations for query text changes in a {@link MaterialSearchView}
 * object. That makes the Observable cold.
 *
 * @author Marc Moreno
 * @version 1.0
 */
final class MaterialSearchViewQueryTextChangesOnSubscribe implements Observable.OnSubscribe<String> {
  final MaterialSearchView view;

  MaterialSearchViewQueryTextChangesOnSubscribe(MaterialSearchView view) {
    this.view = view;
  }

  @Override public void call(final Subscriber<? super String> subscriber) {
    //MainThreadSubscription.verifyMainThread();

    MaterialSearchView.OnQueryTextListener watcher = new MaterialSearchView.OnQueryTextListener() {
      @Override public boolean onQueryTextChange(String s) {
        if (!subscriber.isUnsubscribed()) {
          subscriber.onNext(s);
          return true;
        }
        return false;
      }

      @Override public boolean onQueryTextSubmit(String query) {
        return false;
      }
    };

    view.setOnQueryTextListener(watcher);

    subscriber.add(new MainThreadSubscription() {
      @Override protected void onUnsubscribe() {
        view.setOnQueryTextListener(null);
      }
    });

  }
}