/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.app.eventlist.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.List;
import net.opentrends.m2f.rxmvvm.R;
import net.opentrends.m2f.rxmvvm.databinding.EventItemBinding;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

/**
 * Adapter for {@link Event} objects in a RecyclerView.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventViewHolder> {

  List<Event> eventList;

  public EventListAdapter(List<Event> eventList) {
    this.eventList = eventList;
  }

  public void setEventList(List<Event> eventList) {
    this.eventList = eventList;
    notifyDataSetChanged();
  }

  @Override
  public EventListAdapter.EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    EventItemBinding binding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.event_item,
            parent, false);
    return new EventViewHolder(binding);
  }

  @Override public void onBindViewHolder(EventListAdapter.EventViewHolder holder, int position) {
    holder.bind(eventList.get(position));
  }

  @Override public int getItemCount() {
    return eventList != null ? eventList.size() : 0;
  }

  public class EventViewHolder extends RecyclerView.ViewHolder {

    private EventItemBinding binding;

    public EventViewHolder(EventItemBinding binding) {
      super(binding.getRoot());
      this.binding = binding;
    }

    public void bind(Event event) {
      binding.setEvent(event);
    }
  }
}
