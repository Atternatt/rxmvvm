/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.app.eventlist.rx;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import rx.Observable;

/**
 * Creates a new observable for a {@link MaterialSearchView} text changes.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public final class RxMaterialSearchView {

  /**
   * Creates a {@link Observable<String>} that emits the String changes
   * for {@link MaterialSearchView.OnQueryTextListener}.
   *
   * @param materialSearchView the {@link MaterialSearchView}
   * @return the {@link Observable<String>} that emit text changes
   */
  public static Observable<String> queryTextChangeEvents(MaterialSearchView materialSearchView) {
    return Observable.create(new MaterialSearchViewQueryTextChangesOnSubscribe(materialSearchView));
  }
}
