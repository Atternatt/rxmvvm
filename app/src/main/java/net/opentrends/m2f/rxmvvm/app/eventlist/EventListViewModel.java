/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.app.eventlist;

import android.databinding.ObservableField;
import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import net.opentrends.m2f.rxmvvm.di.ActivityScope;
import net.opentrends.m2f.rxmvvm.domain.interactor.DefaultSubscriber;
import net.opentrends.m2f.rxmvvm.domain.interactor.EventUseCase;
import net.opentrends.m2f.rxmvvm.domain.interactor.UseCaseSubject;
import net.opentrends.m2f.rxmvvm.domain.model.Event;
import net.opentrends.m2f.rxmvvm.retrypolicy.RetryWithConnectivityIncremental;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

/**
 * ViewModel that controls the EventList search.
 *
 * @author Marc Moreno
 * @version 1.0
 */
@ActivityScope public class EventListViewModel {

  public final ObservableField<List<Event>> eventList = new ObservableField<>();
  UseCaseSubject eventListUseCase;

  Observable<String> queryObservable;

  @Inject public EventListViewModel(UseCaseSubject eventListUseCase) {
    this.eventListUseCase = eventListUseCase;
    this.eventListUseCase.setRetryPolicy(new RetryWithConnectivityIncremental(3,10, TimeUnit.SECONDS));
  }

  public void bind(Observable<String> query) {
    queryObservable = query;
    eventListUseCase.bind(queryObservable, new EventListViewModelInteractor());
  }

  /**
   * {@link DefaultSubscriber} implementations that reacts to {@link EventUseCase} emisions.
   */
  @RxLogSubscriber private class EventListViewModelInteractor
      extends DefaultSubscriber<List<Event>> {

    @Override public void onNext(List<Event> events) {
      Log.d("RESULTS", events.size() + "");
      eventList.set(events);
    }

    @Override public void onError(Throwable e) {
      super.onError(e);
    }

    @Override public void onCompleted() {
      super.onCompleted();
    }
  }
}
