/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.di.modules;

import android.content.Context;

import net.opentrends.m2f.rxmvvm.AndroidApplication;
import net.opentrends.m2f.rxmvvm.data.api.RestApi;
import net.opentrends.m2f.rxmvvm.data.api.RestApiImpl;
import net.opentrends.m2f.rxmvvm.data.database.EventDatabase;
import net.opentrends.m2f.rxmvvm.data.database.OrmLiteDatabase;
import net.opentrends.m2f.rxmvvm.data.repository.EventDataRepository;
import net.opentrends.m2f.rxmvvm.domain.executor.PostExecutionThread;
import net.opentrends.m2f.rxmvvm.domain.repository.EventRepository;
import net.opentrends.m2f.rxmvvm.executor.UIThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 *
 * @author Marc Moreno
 * @version 1.0
 */
@Module
public class ApplicationModule {
    private final AndroidApplication application;

    public ApplicationModule(AndroidApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    EventDatabase provideEventDatabase(OrmLiteDatabase database) {
        return database;
    }

    @Provides
    @Singleton
    RestApi provideRestApi(RestApiImpl api) {
        return api;
    }

    @Provides
    @Singleton
    EventRepository provideUserRepository(EventDataRepository eventDataRepository) {
        return eventDataRepository;
    }
}