/*
 * Copyright (c) 2016 Marc Moreno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.opentrends.m2f.rxmvvm.app.eventlist;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import net.opentrends.m2f.rxmvvm.app.eventlist.adapter.EventListAdapter;
import net.opentrends.m2f.rxmvvm.app.eventlist.rx.RxMaterialSearchView;
import net.opentrends.m2f.rxmvvm.domain.model.Event;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;

/**
 * Class that controls the behaviour between view and viewModel. This contains the
 * custom attributes and their interactions using {@link BindingAdapter}.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class EventListViewModelBinding {

  /**
   * This will bind a {@link Observable<String>} from {@link MaterialSearchView} to
   * the {@link EventListViewModel} so this will be able to listen
   * for {@link MaterialSearchView}
   *
   * @param searchView the {@link MaterialSearchView}
   * @param eventListViewModel the {@link EventListViewModel}
   */
  @BindingAdapter("search")
  public static void bindSearchViewToViewModel(MaterialSearchView searchView,
      EventListViewModel eventListViewModel) {

    eventListViewModel.bind(RxMaterialSearchView.queryTextChangeEvents(searchView)
        .debounce(250, TimeUnit.MILLISECONDS));
  }

  /**
   * This will set the list of events to the recyclerview with a {@link EventListAdapter}.
   *
   * @param recyclerView the recyclerview
   * @param oldList the last list setted
   * @param newList the new list to set
   */
  @BindingAdapter("eventList")
  public static void bindListToRecyclerView(RecyclerView recyclerView, List<Event> oldList,
      List<Event> newList) {

    if (oldList == null) {
      recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
      EventListAdapter adapter = new EventListAdapter(newList);
      recyclerView.setAdapter(adapter);
    } else {
      ((EventListAdapter) recyclerView.getAdapter()).setEventList(newList);
    }

  }
}
